package main

import (
	"io/ioutil"
	"net/http"

	w "bitbucket.org/rucuriousyet/ccdatranspiler/cmd/workers"
)

func main() {
	queue := w.NewQueue()

	//This always must be on a goroutine!
	go queue.MakePool(12)

	h := http.NewServeMux()

	h.HandleFunc("/", func(rw http.ResponseWriter, r *http.Request) {
		body, _ := ioutil.ReadAll(r.Body)
		queue.Append(w.Job{DataBuffer: body})
		rw.WriteHeader(200)
	})

	http.ListenAndServe(":8080", h)
}
