package workers

import (
	"fmt"
	"time"

	"bitbucket.org/rucuriousyet/ccdatranspiler/cmd/transpiler"
)

// Action is the expected job output or result
type Action uint

const (
	// StdOut Action
	StdOut Action = 0
	// API Action
	API Action = 1
	// DB Action
	DB Action = 2
)

// Queue is a task queue object
type Queue struct {
	Workers       uint
	Notifications chan Notification
}

// Notification is a basic message
type Notification struct {
	Type uint
	Body interface{}
}

// Job is a basic task to be passed to a worker
type Job struct {
	TimeIn         time.Time
	TimeOut        time.Time
	IncomingFmt    transpiler.DataFmt
	OutgoingFmt    transpiler.DataFmt
	OutgoingAction Action
	DataBuffer     []byte
}

// Worker is a concurrent processing unit
func (q *Queue) Worker(job Job, id uint) {
	fmt.Println("Started:", id)
	time.Sleep(time.Second * 25)
	fmt.Println("Stopped:", id)
	q.Workers--
	q.NotifyComplete()
}

// NewQueue creates new task queue
func NewQueue() Queue {
	return Queue{0, make(chan Notification, 100)}
}

// Append adds a new task or group of tasks to the queue
func (q Queue) Append(jobs ...Job) {
	for _, job := range jobs {
		q.Notifications <- Notification{0, job}
		fmt.Println("Sending job...")
	}
}

// NotifyComplete notifies the queue of available worker
func (q Queue) NotifyComplete() {
	q.Notifications <- Notification{Type: 1}
}

// MakePool creates a new worker pool with n workers.
func (q *Queue) MakePool(maxWorkers uint) {
	fmt.Println("Started new pool...")
	q.Workers = 0
	queue := make([]Job, 0)
	for notif := range q.Notifications {
		if notif.Type == 0 {
			job := notif.Body.(Job)
			// If the queue is empty and workers are available
			if len(queue) < 1 && q.Workers < maxWorkers {
				fmt.Println("tasking immediately.")
				q.Workers++
				id := q.Workers
				go q.Worker(job, id)

				// if the queue has tasks and workers are availble
			} else if q.Workers < maxWorkers {
				fmt.Println("retasking to queued.")
				queue = append(queue, job)
				q.Workers++
				id := q.Workers
				go q.Worker(queue[0], id)
				queue = append(queue[:0], queue[1:]...)

				// if there are no workers
			} else {
				fmt.Println("adding to queue.")
				queue = append(queue, job)
			}
		} else if notif.Type == 1 && q.Workers < maxWorkers && len(queue) > 0 {
			fmt.Println("tasking immediately.")
			q.Workers++
			id := q.Workers
			go q.Worker(queue[0], id)
			//Delete task from queue
			queue = append(queue[:0], queue[1:]...)
		}
		fmt.Println("threads:", q.Workers, "jobs:", len(queue))
	}
}
