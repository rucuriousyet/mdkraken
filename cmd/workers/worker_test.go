package workers

import (
	"testing"
)

func PoolTest(t *testing.T) {
	queue := NewQueue()
	go queue.MakePool(8)
	for i := 0; i < 40; i++ {
		queue.Append(Job{DataBuffer: []byte("apples")})
	}
}

func PoolBench(b *testing.B) {
	queue := NewQueue()
	go queue.MakePool(8)
	for i := 0; i < 40; i++ {
		queue.Append(Job{DataBuffer: []byte("apples")})
	}
}

//
// func EncodeTest(t *testing.T) {
//
// }
//
// func EncodeBench(b *testing.B) {
//
// }
//
// func DecodeTest(t *testing.T) {
//
// }
//
// func DecodeBench(b *testing.B) {
//
// }
