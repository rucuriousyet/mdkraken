package transpiler

// DataFmt represents various available data formats
type DataFmt uint

const (
	// JSON DataFmt
	JSON DataFmt = 0
	// XML DataFmt
	XML DataFmt = 1
	// NFmt DataFmt = No Format
	NFmt DataFmt = 3
)

// Obj is a Basic DTO
type Obj struct {
}
